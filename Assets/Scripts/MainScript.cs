﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainScript : MonoBehaviour {

    bool gameStarted;
    public GameObject tapScreen;

    private Rigidbody2D rigi;

    public Image buttomPos;
    public Sprite playButtom;
    public Sprite pauseButtom;

    private int fatLevel = 1;

    public GameObject tapScreenLoose;
    public GameObject homeButtom;
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    public bool isGrounded;
    private bool pauseGame;
    private bool jumpAble = true;
    private Animator PlayerAnimator;




    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        pauseGame = false;
        tapScreenLoose.SetActive(false);
        homeButtom.SetActive(false);
        gameStarted = false;
        rigi = GetComponent<Rigidbody2D>();
        PlayerAnimator = GetComponent<Animator>();
     
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

       

            if (gameStarted)
            {
            
                Vector3 v = rigi.velocity;
                v.x = 5.0f;
                rigi.velocity = v;
                PlayerAnimator.SetBool("running", true);
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began && isGrounded && jumpAble)
                {
                    Vector3 v2 = rigi.velocity;
                    v2.y = 10f;
                    rigi.velocity = v2;
                }
            }
            }
        if (!gameStarted)
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Ended){
                    gameStarted = true;
                    Destroy(tapScreen);
                    
                }
            }
        }


        
        switch(fatLevel)
        {
            case 2:
                PlayerAnimator.SetBool("mediumRunning", true);
                return;

            case 3:
                PlayerAnimator.SetBool("largeRunning", true);
                return;

            case 4:
                tapScreenLoose.SetActive(true);
                PlayerAnimator.SetBool("xlRunning", true);
                rigi.velocity = new Vector3(0, 0, 0);

                if (Input.touchCount > 0)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                        
                    }
                }
                    return;

        }
       
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "food")
        {
            Destroy(other.gameObject);
            fatLevel++;

        }
        if (other.tag == "Level1End")
        {
            saveGame(2);
            SceneManager.LoadScene("Level2");
        }
        if (other.tag == "Level2End")
        {
            saveGame(3);
            SceneManager.LoadScene("Level3");
        }

        if (other.tag == "Level3End")
        {
            saveGame(4);
            SceneManager.LoadScene("Level4");
        }

        if (other.tag == "Level4End")
        {
            SceneManager.LoadScene("Credits");
        }
    }



    public void TestGame()
    {
        
        if (!pauseGame)
        {
            buttomPos.GetComponent<Image>().sprite = playButtom;
            homeButtom.SetActive(true);
            Time.timeScale = 0;
            pauseGame = true;
            
        }
        else
        {
            buttomPos.GetComponent<Image>().sprite = pauseButtom;
            homeButtom.SetActive(false);
            Time.timeScale = 1;
            pauseGame = false;
        }
    }
    public void makeJump()
    {
        jumpAble = true;
    }
    public void dontJump()
    {
        jumpAble = false;
    }

    public void homeFunction()
    {
        SceneManager.LoadScene("StartMenu");
    }

    public void saveGame(byte value)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Save.dat");
        SaveInfo data = new SaveInfo();
        data.unlocked = value;
        bf.Serialize(file, data);
        file.Close();

    }

}

[Serializable]
public class SaveInfo {
    public byte unlocked;
}

