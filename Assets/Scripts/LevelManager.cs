﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public Image lvl2Color;
    public Image lvl3Color;
    public Image lvl4Color;

    bool lvl2ClickAble = false;
    bool lvl3ClickAble = false;
    bool lvl4ClickAble = false;
    byte unlocked;
    void Start () {
        loadGame();
        if (unlocked == 2)
        {
            lvl2ClickAble = true;
            lvl2Color.GetComponent<Image>().color = new Color(255, 255, 255);
        }
        if (unlocked == 3)
        {
            lvl2ClickAble = true;
            lvl3ClickAble = true;
            lvl2Color.GetComponent<Image>().color = new Color(255, 255, 255);
            lvl3Color.GetComponent<Image>().color = new Color(255, 255, 255);
        }
        if (unlocked == 4)
        {
            lvl2ClickAble = true;
            lvl3ClickAble = true;
            lvl4ClickAble = true;
            lvl2Color.GetComponent<Image>().color = new Color(255, 255, 255);
            lvl3Color.GetComponent<Image>().color = new Color(255, 255, 255);
            lvl4Color.GetComponent<Image>().color = new Color(255, 255, 255);

        }
        

		
	}
	
	// Update is called once per frame
	void Update () {

		
	}


    public void Level1()
    {
        SceneManager.LoadScene("Level1");
        
    }

    public void StartMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }

    public void loadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/Save.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Save.dat", FileMode.Open);
            SaveInfo data = (SaveInfo)bf.Deserialize(file);
            file.Close();
            unlocked = data.unlocked;

        }
    }
    
    public void resetSaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Save.dat");
        SaveInfo data = new SaveInfo();
        data.unlocked = 1;
        bf.Serialize(file, data);
        file.Close();

        lvl2ClickAble = false;
        lvl3ClickAble = false;
        lvl4ClickAble = false;

        lvl2Color.GetComponent<Image>().color = new Color32(84,77,77,255);
        lvl3Color.GetComponent<Image>().color = new Color32(84, 77, 77, 255);
        lvl4Color.GetComponent<Image>().color = new Color32(84, 77, 77, 255);
    }
    public void lvl2Click()
    {
        if(lvl2ClickAble)
        {
            SceneManager.LoadScene("Level2");
        }
    }
    
    public void lvl3Click()
    {
        if (lvl3ClickAble)
        {
            SceneManager.LoadScene("Level3");
        }
    }

    public void lvl4Click()
    {
        if (lvl4ClickAble)
        {
            SceneManager.LoadScene("Level4");
        }
    }

}


